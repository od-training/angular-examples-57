import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input('videoList') videos?: Video[];

  constructor(public svc: VideoDataService) { }
}
