import { Component, OnInit, Input } from '@angular/core';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent {
  constructor(public svc: VideoDataService) { }
}
