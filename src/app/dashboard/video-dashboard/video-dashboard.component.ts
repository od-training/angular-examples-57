import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  
  videos$: Observable<Video[]>;
  
  constructor(svc: VideoDataService) {
    this.videos$ = svc.loadVideos();
  }
}
