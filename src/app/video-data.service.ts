import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

interface ViewDetail {
  age: number;
  region: string;
  date: string;
}

export interface Video {
  id: string;
  title: string;
  author: string;
  viewDetails: ViewDetail[];
}

const apiUrl = 'http://api.angularbootcamp.com';

function embiggenAuthorNames(videos: Video[]) {
  return videos.map(
    v => ({ ...v, author: v.author.toUpperCase() })
  );
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  currentVideo$ = new BehaviorSubject<Video>(null);
  
  constructor(private http: HttpClient) { }

  private setCurrentVideoToFirstVideo(videos: Video[]) {
    if (videos[0]) {
      this.currentVideo$.next(videos[0]);
    }
  }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>(apiUrl + '/videos')
      .pipe(
        tap(data => console.table('Raw video data:', data)),
        map(embiggenAuthorNames),
        tap(data => console.table('Mapped video data:', data)),
        tap(data => this.setCurrentVideoToFirstVideo(data))
      )
    ;
  }
}
